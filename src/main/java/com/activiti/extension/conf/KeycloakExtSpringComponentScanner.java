/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.activiti.extension.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;

/**
 * A means for injecting packages to scan for the Spring context.
 * 
 * @author brian@inteligr8.com
 */
@Configuration
@ComponentScan(
    basePackages = {
        "com.inteligr8.activiti.idm",
        "com.inteligr8.activiti.keycloak",
        "com.inteligr8.activiti.security"
    },
    nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
public class KeycloakExtSpringComponentScanner {

}
